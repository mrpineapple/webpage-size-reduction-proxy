var http = require('http');
var fs = require('fs');
var uuid = require('node-uuid');
var compressjs = require('compressjs');
var bz2 = compressjs.Bzip2;
var exec = require('child_process').exec;

var idid = 0

fs.exists("id.gen",function(exists){
  console.log( ( exists  ? "File is there" : "File is not there" ) );     
  if(exists){
    fs.readFile('id.gen', 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      idid = data;
      console.log("Your ID is: " + idid);
    });
  }else{
    uui = uuid.v4()
    console.log(uui);
    idid =  parseInt(uui,16); //give a random id 0-1000
    fs.writeFile("id.gen", idid, function(err) {
        if(err) {
            console.log(err);
        } else {
            console.log("Your Generated ID is: " + idid);
            console.log("The file was saved!");
        }
    });
  }
}); 

var data = "";
var header = "";

http.createServer(function(request, response) {
    request.headers["fyp_id"] = idid;
    request.headers['Content-Encoding'] = [""];
    request.headers['Accept-Encoding'] =  'gzip,deflate';

 
    var proxy = http.createClient(8383, 'localhost')  //point to our remote proxy
    var proxy_request = proxy.request(request.method, request.url, request.headers);

    proxy_request.addListener('response', function (proxy_response) {
      console.log(proxy_response.headers["content-type"] + ": \t" + proxy_response.headers["content-encoding"])
      header = proxy_response.headers["content-type"];

      if (header == "text/bz2"){
        console.log("BZ2!");        
        proxy_response.headers["content-type"] = "text/html";
        response.writeHead(proxy_response.statusCode, proxy_response.headers);  //needed to remove gzipness
      }else{
         response.writeHead(proxy_response.statusCode, proxy_response.headers);  //needed to remove gzipness
      }

      proxy_response.on('data', function (chunk) {
        if(header == "text/bz2"){
          data += chunk;
        // }else if(header == "text/html"){
        //   data += chunk;
        }else{
          response.write(chunk)
        }
      })
      proxy_response.addListener('end', function() {      
        if (header == "text/bz2"){
          console.log("Bzip2!");

//           fs.writeFile("compressed.fyp", data, function(err) {
//             console.log(data)
//           });  
//         //     exec("python decompress.py bz2",function(err2){
//         //       if(err2) {
//         //         console.log(err2);
//         //       }
//         //       console.log("Python decompression function called.");
//         //       val = fs.readFileSync('decompressed.fyp').toString();
//         //       console.log("Response: " + val);
//         //       response.write(val,'binary');
//         //     });
// //          console.log(data.toString())
//            console.log(data.toString())
//            response.write(data,'binary');
//            data = ""
        }else{
       //    response.writeHead(proxy_response.statusCode, proxy_response.headers);  //needed to remove gzipness
       
           if (header == "text/html"){
//             console.log(data)
             fs.writeFile("fyp.gup", data,{encoding:"ascii"}, function(err) {
             //    console.log("HI")
             //    console.log(data)
             //   if(err) {
             //     console.log(err);
             //   } else {
             //     console.log("Wrote Chunk to File 'fyp.gup'");
             //   }
              });
//             console.log(data.toString())
//             response.write(data.toString(),'binary');
             data = ""
           }
          //do nothing.
        }
        response.end();
      });
    });
    //request stuff
    request.addListener('data', function(chunk) {
      proxy_request.write(chunk, 'binary');
    });
    request.addListener('end', function() {
      proxy_request.end();
    });
}).listen(8080);

