import sys,bz2, gzip
import fileinput

if len(sys.argv) <2:
    sys.stderr.write("Compression Method Not Given or Supported\n")
    
elif sys.argv[1] == "bz2":
    input_file = bz2.BZ2File('compressed.fyp', 'rb')
    try:
        f = open('decompressed.fyp','w+')
        f.write(input_file.read()) # python will convert \n to os.linesep
        f.close()
    finally:
        input_file.close()
elif sys.argv[1] == "c-bz2":
    output = bz2.BZ2File('compressed.fyp', 'wb')
    for line in fileinput.input('test'):
        output.write(line)
    output.close()
elif sys.argv[1] == "gzip":
    a = gzip.open("compressed.fyp.gz")  
    f = open('decompressed.fyp','w+')
    f.write(a.read()) # python will convert \n to os.linesep
    f.close()
elif sys.argv[1] == "c-gzip":
    f = gzip.open('compressed.fyp.gz', 'w+')   #default compression is 9 - aka highest compression ratio
    f.write("hihihihi")
    f.close()
else:
    sys.stderr.write("Compression Method Not Given or Supported\n")