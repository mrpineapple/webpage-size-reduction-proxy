from app import app
from flask import render_template
from flask import request
import cgi

import sqlite3 as lite

con = None

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_to_database()
    return db


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/input',methods=['POST'])
def input():
#    print request.form["butt"]
    c_lev = request.form["slid"]
    ident = request.form["box"]
    ide = request.form["ide"]
    deets = ide.split(",")

    print int(ident),int(deets[0]),int(deets[1]),deets[2],deets[3],int(c_lev)

    con = lite.connect('pref.db')
    
    try:
        cur = con.cursor() 
        cur.execute("CREATE TABLE IF NOT EXISTS Details(ID INT, WIDTH INT, HEIGHT INT, BROWSER TEXT, VERTSION TEXT, C_LEVEL INT);")
        cur.execute("INSERT INTO Details(ID, WIDTH, HEIGHT, BROWSER, VERTSION, C_LEVEL) VALUES(?,?,?,?,?,?);",[int(ident),int(deets[0]),int(deets[1]),deets[2],deets[3],int(c_lev)])
        con.commit()
        con.close() 

    except lite.Error, e:
        if con:
            con.rollback()
        print "Error %s:" % e.args[0]
        sys.exit(1)
        
    finally:    
        print "OKAY" 
        
    return "Information Gathered"
    