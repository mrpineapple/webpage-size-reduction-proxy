import numpy as np
import cv2,re
import time

last = now = start = time.time()

MAX_HEIGHT = 500
MAX_WIDTH = 500

img = cv2.imread('sample_images/fflogo.png')
height, width, depth = img.shape
if (height > MAX_HEIGHT or width > MAX_WIDTH):
    img = cv2.resize(img, (0,0), fx=0.7, fy=0.7) ;
    img = cv2.GaussianBlur(img,(5,5),0)
    now = time.time()
    print "Time to Blur: " + str(now - start)

#tried reductions

def toSVGPath(contour):
    np.set_printoptions(threshold=np.nan,edgeitems=np.inf)
    vals = re.findall(r'[0-9]+',contour)
    point_type = 0
    for index in xrange(len(vals)):
        #MoveTo (x,y)
        if point_type == 0:
            vals[index] = "M" + vals[index] + " "
            point_type = point_type + 1
            continue

        if vals[index] == "Z":
            point_type = 0
            continue

        #LineTo (x,y)
        if point_type == 2 or point_type == 4:
            vals[index] = "L" + vals[index] + " "
            point_type = 3
            continue

        #corresponding y coordinates of above.
        if point_type == 1 or point_type == 3:
            vals[index] = vals[index] + " "
            point_type = point_type + 1
            continue

    res = ''.join(vals)
    if res.count(' ') >= 15:
        return res
    else:
        return None


#kmeans clustering - http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_ml/py_kmeans/py_kmeans_opencv/py_kmeans_opencv.html
Z = img.reshape((-1,3))
# convert to np.float32
Z = np.float32(Z)
# define criteria, number of clusters(K) and apply kmeans()
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
K = 16
ret,label,center=cv2.kmeans(Z,K,None,criteria,1,cv2.KMEANS_PP_CENTERS)
# Now convert back into uint8, and make original image
center = np.uint8(center)
res = center[label.flatten()]
res2 = res.reshape((img.shape))

#col = center
now = time.time()
print "Kmeans: " + str(now - last)
last = now

#cv2.imshow('kmeans',res2)

#connected components
contours = None
con_img = cv2.cvtColor(res2,cv2.COLOR_BGR2GRAY);

#get the different clusters
thresh_lvls = []
for x in range(len(con_img)):
    for y in range(len(con_img[x])):
        if not con_img[x][y] in thresh_lvls:
            thresh_lvls.append(con_img[x][y])
#

now = time.time()
print "Generating Thresh Levels: " + str(now - last)
last = now

f = open("output.svg","w+")
f.write("<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink= \"http://www.w3.org/1999/xlink\" >")

arr = []
col = []
#cv2.imshow("greyscale",con_img)
for itr,t in enumerate(thresh_lvls):
#    ret,thresh = cv2.threshold(con_img,t,t,cv2.THRESH_BINARY)

    thresh = np.zeros((len(con_img),len(con_img[0])), np.uint8)
    
    for x in xrange(len(con_img)):
        for y in xrange(len(con_img[0])):
            if con_img[x][y] == t:
                thresh[x][y] = 255
    
#    cv2.imshow('threshold'+str(itr),thresh)

    contours,hierarchy = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE) 

    # con_img2 = cv2.cvtColor(con_img,cv2.COLOR_GRAY2BGR);
    # img5 = cv2.drawContours(con_img2, contours, -1, (0,255,0), 3)

 #   cv2.imshow('contours'+str(itr),con_img2)

    try:
        test_px = (contours[0][0][0][0],contours[0][0][0][1])
        colour = img[test_px[1]][test_px[0]]
        colour[0],colour[2] = colour[2],colour[0]
        rgb = "rgb(" +str(colour[0])+","+str(colour[1])+","+str(colour[2])+')'

        arr.append(contours)
        col.append(rgb)
    except IndexError:
        print "skipped"

now = time.time()
print "threshold & Contours: " + str(now - last)
last = now



for itr,item in enumerate(arr):
    l = len(arr)
    f.write("<g>")
    for x in arr[l - 1 -itr]:    #have to iterate through first level or numpy supresses the array elements to "..."
        path = toSVGPath(str(x))
        if path == None:
            continue
        rgb = col[l-1-itr]#toRGB(col[l - 1 -itr])
        f.write('<path style="fill:'+ rgb + '"'
            ' stroke="'+rgb + '"'
            ' d="' + path + '" />')
    f.write("</g>")

f.write("</svg>")

now = time.time()
print "Write to File: " + str(now - last)
last = now

now = time.time()
print "Overall: " + str(now - start)
last = now

cv2.waitKey(0)
cv2.destroyAllWindows()