import cStringIO
from PIL import Image

def response(context, flow):
    if flow.response.headers["content-type"] == ["image/png"]:
        s = cStringIO.StringIO(flow.response.content)
        img = Image.open(s).rotate(180)
        s2 = cStringIO.StringIO()
        img.save(s2, "png")
        flow.response.content = s2.getvalue()
    elif flow.response.headers["content-type"] == ["image/jpeg"]:
        print "TEST"
        s = cStringIO.StringIO(flow.response.content)
        img = Image.open(s).rotate(180)
        s2 = cStringIO.StringIO()
        img.save(s2, "jpg")
        flow.response.content = s2.getvalue()


