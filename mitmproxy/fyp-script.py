#!/usr/bin/python
# -*- coding: utf-8 -*-

import cStringIO
import sys
import bz2
import binascii
import re
import sqlite3 as lite
from PIL import Image
from libmproxy import encoding
from slimit import minify

#svg reqs
import numpy as np
import cv2,time

MAX_HEIGHT = 500
MAX_WIDTH = 500

TYPE_OF_COMPRESSION = "BZip2" #not implemented yet


#http://stackoverflow.com/questions/13329445/how-to-read-image-from-in-memory-buffer-stringio-or-from-url-with-opencv-pytho
def create_opencv_image_from_stringio(img_stream, cv2_img_flag=0):
    img_stream.seek(0)
    img_array = np.asarray(bytearray(img_stream.read()), dtype=np.uint8)
    return cv2.imdecode(img_array, cv2_img_flag)

def toSVGPath(contour):
    np.set_printoptions(threshold=np.nan,edgeitems=np.inf)
    vals = re.findall(r'[0-9]+',contour)
    point_type = 0
    for index in xrange(len(vals)):
        #MoveTo (x,y)
        if point_type == 0:
            vals[index] = "M" + vals[index] + " "
            point_type = point_type + 1
            continue

        if vals[index] == "Z":
            point_type = 0
            continue

        #LineTo (x,y)
        if point_type == 2 or point_type == 4:
            vals[index] = "L" + vals[index] + " "
            point_type = 3
            continue

        #corresponding y coordinates of above.
        if point_type == 1 or point_type == 3:
            vals[index] = vals[index] + " "
            point_type = point_type + 1
            continue

    res = ''.join(vals)
    if res.count(' ') >= 15:
        return res
    else:
        return None


def toSVG(image):
    last = now = start = time.time()
    global MAX_HEIGHT
    global MAX_WIDTH

    print "SVG CONVERSION"
    img = create_opencv_image_from_stringio(image,1)
    print "DECODED FROM StringIO"

    height, width, depth = img.shape
    if (height > MAX_HEIGHT or width > MAX_WIDTH):
        img = cv2.resize(img, (0,0), fx=0.7, fy=0.7) ;
        img = cv2.GaussianBlur(img,(5,5),0)
        now = time.time()
        print "Time to Blur: " + str(now - start)

    print "SIZE RESTRICTIONS APPLIED"

    #kmeans clustering - http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_ml/py_kmeans/py_kmeans_opencv/py_kmeans_opencv.html
    Z = img.reshape((-1,3))
    # convert to np.float32
    Z = np.float32(Z)
    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 16
    ret,label,center=cv2.kmeans(Z,K,None,criteria,1,cv2.KMEANS_PP_CENTERS)
    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))

    now = time.time()
    print "Kmeans: " + str(now - last)
    last = now

    #connected components
    contours = None
    con_img = cv2.cvtColor(res2,cv2.COLOR_BGR2GRAY);

    #get the different clusters
    thresh_lvls = []
    for x in range(len(con_img)):
        for y in range(len(con_img[x])):
            if not con_img[x][y] in thresh_lvls:
                thresh_lvls.append(con_img[x][y])

    now = time.time()
    print "Generating Thresh Levels: " + str(now - last)
    last = now

    resultant_svg = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink= \"http://www.w3.org/1999/xlink\" >"

    arr = []
    col = []
    for t in thresh_lvls:
        thresh = np.zeros((len(con_img),len(con_img[0])), np.uint8)
        for x in xrange(len(con_img)):
            for y in xrange(len(con_img[0])):
                if con_img[x][y] == t:
                    thresh[x][y] = 255
        contours,hierarchy = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        try:
            test_px = (contours[0][0][0][0],contours[0][0][0][1])
            colour = img[test_px[1]][test_px[0]]
            rgb = "rgb(" +str(colour[0])+","+str(colour[1])+","+str(colour[2])+')'
            arr.append(contours)
            col.append(rgb)
        except IndexError:
            print "Layer does not exists (No need to worry about this message)."

    now = time.time()
    print "threshold & Contours: " + str(now - last)
    last = now

    for itr,item in enumerate(arr):
        l = len(arr)
        #Can add <g> here for easier debugging
        for x in arr[l - 1 -itr]:    #have to iterate through first level or numpy supresses the array elements to "..."
            path = toSVGPath(str(x))
            if path == None:
                continue
            rgb = col[l-1-itr]
            resultant_svg = resultant_svg + '<path style="fill:'+ rgb + '"' \
                ' stroke="'+rgb + '"' \
                ' d="' + path + '" />'

    resultant_svg += "</svg>"

    now = time.time()
    print "Overall: " + str(now - start)
    last = now

    return resultant_svg
   

def registered(uuid):
    con = lite.connect('../flask-site/microblog-0.1/pref.db') 
    with con:
        cur = con.cursor()
        cur.execute("SELECT COUNT(id) FROM Details WHERE ID="+str(uuid))
        row = cur.fetchall()
    print row[0][0]
    if row[0][0] != 0:
        return True
    else: 
        return False

def compress_level(uuid):
    con = lite.connect('../flask-site/microblog-0.1/pref.db')
    with con:
        cur = con.cursor()
        cur.execute("SELECT c_level FROM DETAILS WHERE ID="+str(uuid))
        row = cur.fetchall()
    return row[0][0]

def parseconditions(conditions,uuid):
    #Get our screen details.
    con = lite.connect('../flask-site/microblog-0.1/pref.db')
    with con:    
        iden = uuid
        cur = con.cursor()   
        #should return 1 entry corresponding to client's browser 
        cur.execute("SELECT * FROM Details WHERE ID=" + str(iden))
        print "BROWSER #" + str(iden)
        row = cur.fetchall()

    for x in conditions:    #those that were split by ','
        result = True
        for y in x:         #those that were split by 'and'
            if y.find("screen") != -1 or y.find("all") != -1 or y.find("device-pixel-ratio") != -1:
                continue

            elif y.find("min-device-width") != -1 or y.find("min-width") != -1:
                if(int(row[0][1]) > int(re.findall('\d+', y)[0]) ):
                    #if our width > value ,  continue
                    continue
                else:
                    #else set result false and break
                    result = False
                    break
            elif y.find("min-device-height") != -1 or y.find("min-height") != -1:
                if(int(row[0][2]) > int(re.findall('\d+', y)[0]) ):
                    continue
                else:
                    result = False
                    break
            elif y.find("max-device-width") != -1 or y.find("max-width") != -1:
                if(int(row[0][1]) < int(re.findall('\d+', y)[0]) ):
                    #if our width > value ,  continue
                    continue
                else:
                    #else set result false and break
                    result = False
                    break
            elif y.find("max-device-height") != -1 or y.find("max-height") != -1:
                if(int(row[0][2]) < int(re.findall('\d+', y)[0]) ):
                    continue
                else:
                    result = False
                    break
            elif y.find("aspect-ratio") != -1:
                given = (int(row[0][1])/int(row[0][2]))
                our = re.findall('\d+', y)
                ourratio = our[0]/our[1]
                if(given < ourratio + 0.05 and given > ourratio -0.05 ):
                    continue
                else:
                    result = False
                    break
            elif y.find("orientation") != -1:
                our = map(int, re.findall(r'\d+', y))
                if (y.find("landscape") != -1 and our[0] >our[1]):
                    continue
                elif (y.find("portrait") != -1 and our[1] >our[0]):
                    continue
                else:
                    result = False
                    break
            else:    #its unlikely we would actually use any others
                result = False
                break
    return result

def response(context, flow):

    #If not registered tell them to register
    is_registered = registered(flow.uuid)
    if not is_registered:
        if (flow.response.headers["content-type"] == ["text/html"]):
            flow.response.content = ("Please use the chrome extension to register your browser with this service. Your Id can be seen when starting the local proxy\n clear your cache and reload after registering to see this page's content")
        return

    compress = compress_level(flow.uuid) 
    
    if compress == 0:
        return

    #svg:
    if (flow.response.headers["content-type"] == ["image/png"] or flow.response.headers["content-type"] == ["image/jpeg"]) and compress >=4:
        s = cStringIO.StringIO(flow.response.content)
        try:
            s2 = toSVG(s)
            flow.response.content = s2
            flow.response.headers["content-type"] = ["image/svg+xml"]
            print "Successful vector conversion"
        except e:
            print "Errored during vector conversion"
            print e


        sys.stdout.flush()
        sys.stderr.flush()
        
    #images
    elif flow.response.headers["content-type"] == ["image/png"] or flow.response.headers["content-type"] == ["image/jpeg"] and (compress == 2 or compress == 3):# or flow.response.headers["content-type"] == ["image/gif"]:
        f = open('logfile.txt', 'a')
        s = cStringIO.StringIO(flow.response.content)
        img = Image.open(s)
        f.write(img.format + ": ")
        f.write(str(sys.getsizeof(s.getvalue())))
        f.write(" -> ")
        (imgwidth, imgheight) = img.size
        s2 = None   

        if flow.response.headers["content-type"] == ["image/jpeg"]:
            img = img.resize((imgwidth/2, imgheight/2), Image.NEAREST)
            img = img.resize((imgwidth, imgheight), Image.NEAREST)
            s2 = cStringIO.StringIO()
            img.save(s2,"JPEG" ,optimize=True,quality=75)
        elif flow.response.headers["content-type"] == ["image/png"]:
            img = img.resize((imgwidth/2, imgheight/2), Image.NEAREST)
            img = img.resize((imgwidth, imgheight), Image.NEAREST)
            s2 = cStringIO.StringIO()
            img.save(s2,"png",optimize=True,bits=16)
        # elif flow.response.headers["content-type"] == ["image/gif"]:
        #     img = img.resize((imgwidth/2, imgheight/2), Image.NEAREST)
        #     img = img.resize((imgwidth, imgheight), Image.NEAREST)
        #     s2 = cStringIO.StringIO()
        #     img.save(s2,"png",optimize=True,bits=16)
        f.write(str(sys.getsizeof(s2.getvalue())))
        flow.response.content = s2.getvalue()

        f.write("\n")
        f.close()

    #text
    elif flow.response.headers["content-type"] == ["text/html"] and (compress == 3 or compress == 5): #or flow.response.headers["content-type"] == ["text/html; charset=UTF-8"] :
        f = open('logfile.txt', 'a')
        f.write("HTML: ")
        f.close()

        html = ""
        if not flow.response.headers["Content-Encoding"] or flow.response.headers["Content-Encoding"][0] == "":
            #normal
            f = open('logfile.txt', 'a')
            f.write("~")
            f.close()
            html = flow.response.content
        else:
            f = open('logfile.txt', 'a')
            f.write("@")
            f.close()
            html = flow.response.get_decoded_content()

        f = open('logfile.txt', 'a')
        f.write(str(len(html)))
        f.write(" -> ")
        f.close()

        #Get our screen deets.
        con = lite.connect('../flask-site/microblog-0.1/pref.db')
        with con:    
            iden = 591
            cur = con.cursor()   
            cur.execute("SELECT * FROM Details WHERE ID=" + str(iden))
            row = cur.fetchall()

        if row[0][3] == "Internet Explorer":
            #make html comment conditionals solid
            html = re.sub(r'<!--\[if .+>.*\[endif\]-->)','',html)


        html = re.sub(r'>\s+<','> <',html)
        html = re.sub(r'(<!--([\s\S]*?)-->|\s\s+)','',html) #need detection for inside of <script>s

	
        if not flow.response.headers["Content-Encoding"] or flow.response.headers["Content-Encoding"][0] == "" or flow.response.headers["Content-Encoding"][0] == "undefined":
           

           #s2 = cStringIO.StringIO()
           #data = bz2.compress(html)
           #s2.write(data)
           
           #fil = open("compression.test.bz2","w")
           #fil.write(s2.getvalue())
           #fil.close()
           
           #flow.response.content = s2.getvalue()
           flow.response.content = encoding.encode("gzip", html)
           flow.response.headers["Content-Encoding"] = ["gzip"]
           #flow.response.headers["content-type"] = ["text/g2"]


           print "COMPRESSED GZip"	

        f = open('logfile.txt', 'a')
        f.write(str(len(html)))    
        f.write(":")
        f.close()

        f = open('logfile.txt', 'a')
        f.write(str(len(html)))    
        f.write("\n")
        f.close()


    elif flow.response.headers["content-type"] == ["application/javascript"]:
        pass
#         f = open('logfile.txt', 'a')
#         s = cStringIO.StringIO(flow.response.content)
#         f.write("JS: ")
#         newjs = ""
#         if not flow.response.headers or flow.response.headers["Content-Encoding"][0] == "":
#             newjs = flow.response.content
#         else:
#             newjs = flow.response.get_decoded_content()
#         f.write(str(len(newjs)))

#         f.write(" -> ")
#         #strip comments
# #        newjs = minify(newjs, mangle=True, mangle_toplevel=True)
# #        newjs = re.sub("(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)","",newjs)
#         if not flow.response.headers or flow.response.headers["Content-Encoding"][0] == "":
#             flow.response.content = newjs 
#         else:
#             flow.response.content = encoding.encode("gzip", newjs)  

#         f.write(str(len(newjs)))
#         f.write("\n")
#         f.close()

    elif flow.response.headers["content-type"] == ["text/css"] and (compress == 3 or compress == 5):
        f = open('logfile.txt', 'a')
        f.write("CSS: ")
        f.close()

        if not flow.response.headers["Content-Encoding"] or flow.response.headers["Content-Encoding"][0] == "":
            newcss = flow.response.content
        else:
            newcss = flow.response.get_decoded_content()


        newcss = flow.response.get_decoded_content()
        f = open('logfile.txt', 'a')
        f.write(str(len(newcss)))
        f.close()
        found = 0

        #strip comments
        newcss = re.sub("(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([^:]//.*)","",newcss)

        for x in xrange(newcss.count('@media')):

            found = found + 1
            start = newcss.find('@media')
            op = newcss.find('{',start)

            brackets = 1
            for index,c in enumerate(newcss[op+1:]):


                if brackets == 0:
                    #evaluate conditions to what we will accept.
                    conditions = newcss[start:op]

                    cond = conditions.split(',')
                    c = []
                    for x in cond:
                        c  = c + [x.split('and')]

                    if parseconditions(c,flow.uuid) == False:
                        newcss = newcss.replace(newcss[start:op+1+index],'')
                        break
                    else:
                        break
                elif c == '{':
                    brackets = brackets + 1
                elif c == '}':
                    brackets = brackets - 1

        #Get our screen deets.
        con = lite.connect('../flask-site/microblog-0.1/pref.db')
        with con:    
            iden = 591
            cur = con.cursor()   
            #should return 1 entry 
            cur.execute("SELECT * FROM Details WHERE ID=" + str(iden))
            row = cur.fetchall()

        #browser specific replacements
        if row[0][3] != u"Chrome":
            newcss = re.sub(r'(-webkit-.+?)(:(.+?);|\((.+?);)','',newcss)
        if row[0][3] != u"Firefox":
            newcss = re.sub(r'(-moz-.+?)(:(.+?);|\((.+?);)','',newcss)

        f = open('logfile.txt', 'a')
        f.write(" -> ")
        f.write(str(len(newcss)) + "\n")
        f.close()
        
        if not flow.response.headers["Content-Encoding"] or flow.response.headers["Content-Encoding"][0] == "":
            flow.response.content = newcss 
        else:
            flow.response.content = encoding.encode("gzip", newcss) 


    elif flow.response.headers["content-type"] == ["text/html"] and (compress == 1):
        #just gzip 
        if not flow.response.headers["Content-Encoding"] or flow.response.headers["Content-Encoding"][0] == "" or flow.response.headers["Content-Encoding"][0] == "undefined":
            flow.response.content = encoding.encode("gzip", html)
            flow.response.headers["Content-Encoding"] = ["gzip"]

    sys.stdout.flush()
    sys.stderr.flush()




def request(context,flow):
    unique_id = int(flow.request.headers["fyp_id"][0])
    del flow.request.headers["fyp_id"]  #dont send to servers
    flow.uuid = unique_id               #attach to the flow object
    sys.stdout.flush()
    
