var in_view = false;
var resort = {};
var allImages = document.getElementsByTagName('img');
var element = null;
var scrollTop = document.documentElement.scrollTop;
var bHeight = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;
var scrollBottom = scrollTop + bHeight;

for (var i = 0; i < allImages.length; i++){
    element = allImages[i].src.replace(/[?]/g,'');
    imgTop = allImages[i].offsetTop;    
    if(imgTop < scrollBottom || resort[element]){   //if in view, or if we've already loaded it and it's in memory
        resort[element] = true;
    }else{
        resort[element] = false;
    }
}
resort;

chrome.runtime.sendMessage({result: resort});
