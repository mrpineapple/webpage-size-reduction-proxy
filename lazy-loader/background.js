/*

  Lazy Loading of Images

*/
var resources = {};
blap = function(e){
    return {cancel:true};
}



initial = chrome.webRequest.onBeforeRequest.addListener(blap,{urls: ["http://*/*", "https://*/*"],types: ["image"]}, ["blocking"])

chrome.runtime.onMessage.addListener(function(message,sender,sendResponse){
    resources = message.result;
});
chrome.tabs.executeScript(null,{file:"index.js"},function(res){
    //replace content listener with our own.
    chrome.webRequest.onBeforeRequest.removeListener(blap);
    chrome.webRequest.onBeforeRequest.addListener(function(e){
        if(e.type == "image"){
            link = e.url.replace(/[?]/g,'');;
            //remove "?"s
            if (resources[link] === undefined || resources[link] == true){
                return {};
            }else{
                return {cancel:true};
            }
        }else{
            count = 0;
            for(var key in resources){
                if(!resources[key])
                    count++;
            }
            console.log("calls saved on previous url: " + count);
        }
    },{urls: ["http://*/*", "https://*/*"],types: ["image","main_frame"]}, ["blocking"])
    //refresh our images
    setInterval(function(){chrome.tabs.executeScript(null,{file:"refresh.js"})},500);
});

